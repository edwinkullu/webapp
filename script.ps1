
git clone https://github.com/edwinkullu/Demo1.git
# Change directory to your local repository
Set-Location -Path "C:\Users\edwin.k\OneDrive - iWork Technologies Pvt. Ltd\Desktop\Test"

# Initialize a new Git repository (if not already initialized)
git init

# Add the files to the Git repository
git add .

# Commit the changes with a meaningful message
git commit -m "Initial commit"

# Add the remote Git repository URL
git remote add origin https://gitlab.com/edwinkullu/webapp.git

# Push the code to the Git repository (specifying the branch)
git push -u origin master
